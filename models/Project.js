const mongoose = require('mongoose')

const ProjectSchema = mongoose.Schema({
    name : {
        type: String,
        required: true
    },
    user_id : {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    description : String,
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Projects", ProjectSchema);