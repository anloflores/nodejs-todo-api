const mongoose = require('mongoose')

const TokenSchema = mongoose.Schema({
    token : {
        type: String,
        required: true,
        unique: true,
    },
    
    user_id : {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    
    type : {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Tokens", TokenSchema);