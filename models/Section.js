const mongoose = require('mongoose')

const SectionSchema = mongoose.Schema({
    name : {
        type: String,
        required: true
    },
    project_id : {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    description : String,
    status: {
        type: Number,
        default: 0
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Sections", SectionSchema);