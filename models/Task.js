const mongoose = require('mongoose')

const TaskSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  project_id: {
    type: mongoose.Schema.Types.ObjectId,
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  section_id: {
    type: mongoose.Schema.Types.ObjectId,
  },
  task_id: {
    type: mongoose.Schema.Types.ObjectId,
  },
  description: String,
  status: {
    type: Number,
    default: 0,
  },
  deadline: Date,
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Tasks", TaskSchema);