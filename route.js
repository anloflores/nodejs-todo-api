const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

// Import Files from Routes Folder
const auth = require('./routes/auth')
const project = require("./routes/project");
const task = require("./routes/task");

router.use("/auth", auth);
router.use("/project", authenticated, project);
router.use("/task", authenticated, task);


async function authenticated(req, res, next) {
    let authorization = req.headers["authorization"];
    let accessToken = authorization.split(" ")[1];
    
    await jwt.verify(accessToken, process.env.ACCESS_SECRET_TOKEN, (err, user) => {
        if(err) res.sendStatus(403);
        req.user = user;
        next();
    });
}

module.exports = router;
