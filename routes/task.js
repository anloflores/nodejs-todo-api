const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const Task = require('../models/Task')
const Project = require('../models/Project')
const Section = require('../models/Section')

router.get('/user', async (req, res) => {
    Task.aggregate([
        {
            $graphLookup: {
                from: "tasks",
                startWith: "$_id",
                connectFromField: "_id",
                connectToField: "task_id",
                as: "sub_task",
                maxDepth: 3,
            },
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user'
            },
        },
        {
            $lookup: {
                from: 'projects',
                localField: 'project_id',
                foreignField: '_id',
                as: 'project'
            },
        },
        {
            $match: {
                task_id : {
                    $eq : null
                },
                user_id : {
                    $eq : mongoose.Types.ObjectId(req.user._id)
                }
            }
        },{
            $project: {
                'user.password': 0
            }
        },
    ])
    .exec((err, result) => {
        if (err) res.send(err);
        res.send(result);
    });
})

router.get('/project', async (req, res) => {
    Task.aggregate([
        {
            $graphLookup: {
                from: "tasks",
                startWith: "$_id",
                connectFromField: "_id",
                connectToField: "task_id",
                as: "sub_task",
                maxDepth: 3,
            },
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user'
            },
        },
        {
            $lookup: {
                from: 'projects',
                localField: 'project_id',
                foreignField: '_id',
                as: 'project'
            },
        },
        {
            $match: {
                task_id : {
                    $eq : null
                },
                project_id : {
                    $eq : mongoose.Types.ObjectId(req.body.project_id)
                }
            }
        },{
            $project: {
                'user.password': 0
            }
        },
    ])
    .exec((err, result) => {
        if (err) res.send(err);
        res.send(result);
    });
})

router.post('/store', async (req, res) => {
    req.body.user_id = req.user._id;
    let task = new Task(req.body);

    var sectionExists = true;
    if(req.body.project_id !== null) {
        await Project.findById(req.body.project_id, async (err, project) => {
            if(err) return res.sendStatus(422);
            if(data.section_id != 0) {
                sectionExists = false 

                await Section.findById(req.body.section_id, (err, section) => {
                    if(err) return res.sendStatus(422);
                    sectionExists = true;
                });
            }
        });
    }
    
    if(sectionExists) {
        task.save()
        .then(data => {
            res.json(data);
        }).catch(err => {
            res.json({
                err : true,
                msg : err
            })
        })
    }
})

router.patch('/update', async (req, res) => {
    let data = {
        name: req.body.name,
        description: req.body.description,
        section_id: req.body.section_id || 0,
        project_id: req.body.project_id,
        deadline: req.body.deadline,
        updated_at: Date.now()
    };

    var sectionExists = true;  
    await Project.findById(data.project_id, async (err, project) => {
        if(err) return res.sendStatus(422);
        
        if(data.section_id != 0) {
            sectionExists = false 

            await Section.findById(data.section_id, (err, section) => {
                if(err) return res.sendStatus(422);
                sectionExists = true;
            });
        }
    });

    if(sectionExists) {
        const task = await Task.updateOne({ _id: req.body.id }, { $set: data });
        res.json(task);
    } else {
        return res.sendStatus(422);
    }
});

router.delete('/delete', async (req, res) => {
    await Task.deleteOne({ _id: req.body.id }, (err) => {
        if (err) return res.sendStatus(404);
        res.sendStatus(204);
    });
})

module.exports = router;