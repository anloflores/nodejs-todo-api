const express = require('express');
const router = express.Router();

const Project = require('../models/Project')

router.get('/', async (req, res) => {
    let projects = await Project.find({ user_id: req.user._id});
    res.status(200).json(projects);
})

router.post('/store', async (req, res) => {
    req.body.user_id = req.user._id;
    let project = new Project(req.body);
    
    project.save()
        .then(data => {
            res.json(data);
        }).catch(err => {
            res.json({
                err : true,
                msg : err
            })
        })
})

router.patch('/update', async (req, res) => {
    let data = {
        name: req.body.name,
        description: req.body.description
    };

    const project = await Project.updateOne({_id : req.body.id}, { $set: data})

    res.json(project);
});

router.delete('/delete', async (req, res) => {
    await Project.deleteOne({ _id: req.body.id }, (err) => {
        if (err) return res.sendStatus(404);
        res.sendStatus(204);
    });
})

module.exports = router;