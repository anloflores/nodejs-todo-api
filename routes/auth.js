const express = require('express')
const router = express.Router();

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

// Schema
const User = require('../models/User');
const Token = require('../models/Token');

router.post('/login', (req, res) => {
    if(req.body.email == '' || req.body.password == '') return res.sendStatus(422);

    User.findOne({ email: req.body.email }, async function(err, user) {
        if(err || user == null) return res.sendStatus(404)

        let checkPassword = bcrypt.compareSync(req.body.password, user.password);
        if(!checkPassword) return res.sendStatus(422);

        let accessToken = await jwt.sign(user.toJSON(), process.env.ACCESS_SECRET_TOKEN, {
          expiresIn: "1440m",
        });

        let refreshToken = await jwt.sign(user.toJSON(), process.env.REFRESH_SECRET_TOKEN);

        let access_token = new Token({
            token: accessToken,
            user_id: user._id,
            type: 'access'
        });
        await access_token.save();

        let refresh_token = new Token({
            token: accessToken,
            user_id: user._id,
            type: 'refresh'
        })
        await refresh_token.save();

        user.password = null;
        
        res
          .json({
            user: user,
            token: {
              prefix: "Bearer",
              access_token: accessToken,
              refresh_token: refreshToken,
            },
          })
    });

    // res.sendStatus(500)

})

router.post('/register', async (req, res) => {
    let user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password && bcrypt.hashSync(req.body.password, saltRounds)
    });

    user.save()
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            res.json({
                err : true,
                msg : err
            })
        })
})

module.exports = router;