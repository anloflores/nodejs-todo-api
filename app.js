require('dotenv').config();

const express = require('express')
const app = express()

var http = require("http");
const https = require("https");
const fs = require("fs");

var cors = require("cors");
const cookieParser = require("cookie-parser");
const mongoose = require('mongoose')
mongoose.set("useCreateIndex", true);

var corsOptions = {
  origin: "http://127.0.0.1:5500",
  credentials: true,
};

app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());

// Routes
const routes = require('./route');
app.use('/v1', routes);


// Connect to DB
mongoose.connect(process.env.MONGO_DB, { useNewUrlParser: true, useUnifiedTopology: true })
app.listen(3000);